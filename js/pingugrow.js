// Pingu Grow JS
// by R&R
// based on the Bubbles 2 prototype by Emanuele Feronato
// September 2013
// under BY-NC-SA CC license
// http://creativecommons.org/licenses/by-nc-sa/3.0/us/

window.pingugrow = {};

(function (){

var interval,
    FPS = 24,
    m_canvasOffsetTop = 0,
    m_canvasOffsetLeft = 0,
    bgPos = 0,
    muted = false;


// ************************************
// Class Ball
//              The user will control the ball movements 
// with the control keys.
// ************************************
function Ball()
{
        // CONSTRUCTOR
        this.x = 0;
        this.y = 0;
        this.vx = 0;
        this.vy = 0;
        this.radius = 10;
        this.color = "#000000";
        this.iterator =0;
        this.rotation =0;

        // ------------------------------------
        // Ball reset
        this.ResetBall = function() 
        {
                this.x = 0;
                this.y = 0;
                this.vx = 0;
                this.vy = 0;
                this.radius = 10;
                this.color = "#000000";
                this.iterator =0;
                this.rotation =0;
        }
        
        // ------------------------------------
        // Ball's logic
        this.Update = function() 
        {
                if (m_mouseDown)
                {
                        this.vx=(m_mousePositionX-this.x)/5;                    
                        this.vy=(m_mousePositionY-this.y)/5;
                }
                // PROCESS INPUT
                if (m_keyPressedLeft)   this.vx--;
                if (m_keyPressedRight)  this.vx++;
                if (m_keyPressedUp)     this.vy--;
                if (m_keyPressedDown)   this.vy++;

                // BALL'S LOGIC
                this.x+=this.vx;
                this.y+=this.vy;
                this.vx*=friction;              
                this.vy*=friction;
                
                // CONTROL LIMITS
                if (this.x<this.radius) this.x=this.radius;
                if (this.y<this.radius) this.y=this.radius;
                if (this.x+this.radius>SCREEN_WIDTH)    this.x=SCREEN_WIDTH-this.radius;
                if (this.y+this.radius>SCREEN_HEIGHT)   this.y=SCREEN_HEIGHT-this.radius;

                // ROTATION
                if(m_timeoutGravityWell > 0) this.rotation -= 0.12;
                else this.rotation += 0.04;
                while(this.rotation > 2*Math.PI) this.rotation -= 2*Math.PI;
                while(this.rotation < 0) this.rotation += 2*Math.PI;
        }

        // ------------------------------------
        // Ball's Render
        this.Render = function() 
        {
                // FILL AREA
                /*
                m_context.beginPath();
                m_context.fillStyle=this.color;
                m_context.arc(this.x,this.y,this.radius,0,Math.PI*2,true);
                m_context.closePath();
                m_context.fill();
                */

                // DRAW BUBBLE
                m_context.save();
                m_context.translate( this.x, this.y );
                m_context.rotate( this.rotation );
                if( m_timeoutInvulnerable > 0 && m_indexInvulnerable++ > m_pulseInvulnerable) {
                  m_context.drawImage(m_bubbleImage, -this.radius, -this.radius, this.radius*2, this.radius*2);
                } else
                  m_context.drawImage(m_pinguImage, -this.radius, -this.radius, this.radius*2, this.radius*2);
                if(m_indexInvulnerable > 2 * m_pulseInvulnerable) m_indexInvulnerable = 0;
                m_context.restore();
        }
}

// ************************************
// Class Item
//        Represent the items that the user's ball can collide to
// ************************************
function Item(type)
{
        // var TYPE_ITEM_BUBBLE         = 1;
        // var TYPE_ITEM_ENEMY          = 2;
        // var TYPE_ITEM_POWERUP        = 3;

        // CONSTRUCTOR
        this.type = type;
        this.subtype = 0;
        this.x = 0;
        this.y = 0;
        this.vx = 0;
        this.vy = 0;
        this.defaultvy = 0;
        this.radius = 0;
        this.color = "#000000";
        this.iterator = 0;
        this.isAlive = false;
        this.spin = 0;
        this.rotation = 0;

        // ------------------------------------
        // Item Initialitzation
        this.Init = function(radiusIni, radiusEnd, speedIni, speedEnd) 
        {
                this.isAlive = true;
                this.radius = radiusIni+Math.floor(Math.random()*(radiusEnd-radiusIni));
                this.x = Math.floor(Math.random()*SCREEN_WIDTH);
                this.y = 0;
                this.vx = 0;
                this.vy = speedIni+Math.floor(Math.random()*(speedEnd-speedIni));
                this.defaultvy = this.vy;
                if(this.type == TYPE_ITEM_ENEMY) {
                  this.subtype = Math.floor(Math.random() * m_virusImages.length);
                  this.spin = 0.1 * Math.random();
                } else if(this.type == TYPE_ITEM_POWERUP) {
                  this.subtype = Math.floor(Math.random() * m_msdImages.length);
                }
                this.rotation = 0;
                
                // alert("Item::Init: Initialitzation of a bubble::this.radius="+this.radius);
        }

        
        // ------------------------------------
        // Reset the object to its initial values
        this.ResetItem = function() 
        {       
                this.x = 0;
                this.y = 0;
                this.vx = 0;
                this.vy = 0;
                this.isAlive = false;
                if(this.type == TYPE_ITEM_ENEMY) {
                  this.subtype = Math.floor(Math.random() * m_virusImages.length);
                  this.spin = 0.1 * Math.random();
                  if(Math.random() > -0.5) this.spin = (-1) * this.spin;
                } else if(this.type == TYPE_ITEM_POWERUP) {
                  this.subtype = Math.floor(Math.random() * m_msdImages.length);
                }
                this.rotation = 0;
        }
        
        // ------------------------------------
        // Check if the bubble collide with the bubble of the player
        this.IsCollision = function() 
        {
                if (this.isAlive)
                {
                        if (Math.abs(this.x-m_ball.x)+Math.abs(this.y-m_ball.y)<m_ball.radius+this.radius)
                        {
                                switch (this.type)
                                {
                                        case TYPE_ITEM_BUBBLE:
                                                var increaseScore = Math.floor(this.radius/5);
                                                m_score += (increaseScore * 100);
                                                $('.score-text').text(m_score);
                                                m_ball.radius += increaseScore;
                                                if(!muted && typeof(FlashCanvas) == "undefined") jsfx_samples.bubble.play();
                                                this.y += SCREEN_HEIGHT;
                                                break;
                                                
                                        case TYPE_ITEM_ENEMY:
                                                if(m_timeoutInvulnerable <= 0) {
                                                  if(m_ball.radius <= (2 * this.radius) + 10) {
                                                    if(!muted && typeof(FlashCanvas) == "undefined") jsfx_samples.explosion.play();
                                                    m_askToLose = true;
                                                  } else {
                                                    if(!muted && typeof(FlashCanvas) == "undefined") jsfx_samples.hit.play();
                                                    m_ball.radius -= 2 * this.radius;
                                                    m_timeoutInvulnerable = 400;
                                                    m_indexInvulnerable = 0;
                                                  }
                                                  this.y += SCREEN_HEIGHT;
                                                }
                                                break;

                                        case TYPE_ITEM_POWERUP:
                                                if(!muted && typeof(FlashCanvas) == "undefined") jsfx_samples.powerup.play();
                                                switch(this.subtype) {
                                                        case 0:
                                                                /* Powerup 0, kill all enemies */
                                                                for (i=0;i<TOTAL_NUMBER_ENEMIES;i++)
                                                                {
                                                                        m_enemies[i].y += SCREEN_HEIGHT;
                                                                }
                                                                m_timeoutZap = 500;
                                                                m_indexZap = 0;
                                                                break;
                                                        case 1:
                                                                /* Powerup 1, invulnerale for 4 seconds */
                                                                m_timeoutInvulnerable = 4000;
                                                                m_indexInvulnerable = 0;
                                                                break;
                                                        case 2:
                                                                /* Powerup 2, gravity well for 6 seconds */
                                                                m_timeoutGravityWell = 6000;
                                                                break;
                                                        case 3:
                                                                /* Powerup 3, bullettime for 6 seconds */
                                                                m_timeoutBulletTime = 6000;
                                                                break;
                                                }
                                                this.y += SCREEN_HEIGHT;
                                                break;
                                }
                        }
                }
        }
        // ------------------------------------
        // Item's Render
        this.Update = function()
        {
                if (this.isAlive)
                {
                        // BALL'S LOGIC
                        if(this.type == TYPE_ITEM_BUBBLE &&
                           m_timeoutGravityWell > 0 &&
                           Math.abs(m_ball.x - this.x) < m_gravityWellThreshold &&
                           Math.abs(m_ball.y - this.y) < m_gravityWellThreshold) {
                             this.vx += (m_ball.x - this.x) * m_gravityWellMultiplier;
                             this.vy += (m_ball.y - this.y) * m_gravityWellMultiplier;
                        } else {
                          this.vx = 0;
                          this.vy = this.defaultvy;
                        }
                        this.x+=(m_timeoutBulletTime > 0 ? this.vx * m_bulletTimeMultiplier : this.vx);
                        this.y+=(m_timeoutBulletTime > 0 ? Math.max(this.vy * m_bulletTimeMultiplier, 1) : this.vy);

                        // CHECK COLLISION WITH USER BALL
                        this.IsCollision();

                        // ROTATION
                        this.rotation += this.spin;
                        while(this.rotation > 2*Math.PI) this.rotation -= 2*Math.PI;
                        while(this.rotation < 0) this.rotation += 2*Math.PI;

                        // CONTROL LIMITS
                        if (this.y-this.radius<SCREEN_HEIGHT)
                        {
                                this.isAlive = true;
                        }
                        else
                        {
                                this.isAlive = false;
                        }
                }
        }

        // ------------------------------------
        // Item's Render
        this.Render = function() 
        {
                if (this.isAlive)
                {
                        switch (this.type)
                        {
                                case TYPE_ITEM_BUBBLE:
                                        // DRAW BUBBLE
                                        m_context.drawImage(m_bubbleImage, this.x-this.radius, this.y-this.radius, this.radius*2, this.radius*2);                                       
                                        break;
                                        
                                case TYPE_ITEM_ENEMY:
                                        // DRAW VIRUS
                                        m_context.save();
                                        m_context.translate( this.x, this.y );
                                        m_context.rotate( this.rotation );
                                        m_context.drawImage(m_virusImages[this.subtype], -this.radius, -this.radius, this.radius*2, this.radius*2);                                       
                                        m_context.restore();
                                        break;

                                case TYPE_ITEM_POWERUP:
                                        // DRAW MSD LOGO
                                        m_context.drawImage(m_msdImages[this.subtype], this.x-this.radius, this.y-this.radius, this.radius*2, this.radius*2);                                       
                                        break;
                        }
                }
        }
}



// ************************************
// Class MyText
//              A button with text
// ************************************
function MyText(text, x, y, height, color, font, align, baseline, enableBackground)
{
        // CONSTRUCTOR
        this.x = x;
        this.y = y;
        this.text = text;
        this.color = color;

        this.font = font;
        this.textAlign = align;
        this.textBaseline = baseline;
        
        this.heightText = height;
        
        // ------------------------------------
        // Check if the point is inside the area
        this.IsInside = function(x,y) 
        {
                switch (this.textAlign)
                {
                        case 'left':
                                return ((this.x-5<x)&&(this.y-5<y)&&(this.x+this.widthText+10>x)&&(this.y+this.widthText+10>y));

                        case 'center':
                                return ((this.x-5-(this.widthText/2)<x)&&(this.y-5<y)&&(this.x+this.widthText+10>x)&&(this.y+this.widthText+10>y));
                                
                        default:
                                return ((this.x-5<x)&&(this.y-5<y)&&(this.x+this.widthText+10>x)&&(this.y+this.widthText+10>y));
                                break;
                }
        }

        // ------------------------------------
        // Render the text
        this.Render = function() 
        {
                // ++ RENDER ++
                m_context.font = this.font;
                m_context.textAlign = this.textAlign;
                m_context.textBaseline = this.textBaseline;

                this.sizeText = m_context.measureText(this.text);
                this.widthText = this.sizeText.width;
                
                // DRAW RECTANGLE IN THE BACKGROUND OF TEXT
                if (enableBackground)
                {
                        m_context.beginPath();
                        m_context.strokeStyle = '#000000';
                        m_context.fillStyle = this.color;
                        switch (align)
                        {
                                case 'left':
                                        m_context.rect(this.x-5, this.y-5, this.widthText+10, this.heightText+10);              
                                        break;

                                case 'center':
                                        m_context.rect(this.x-5-(this.widthText/2), this.y-5, this.widthText+10, this.heightText+10);
                                        break;
                                        
                                case 'right':
                                        break;
                        }
                        m_context.fill();
                        m_context.stroke();
                }
                
                // DRAW TEXT
                if (enableBackground)
                {
                        m_context.fillStyle = '#000000';
                }
                else
                {
                        m_context.fillStyle = this.color;
                }
                m_context.fillText(this.text, this.x, this.y);
                m_context.closePath();
        }
}


// ++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++
// GLOBAL CODE
// ++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++

// ++ CONSTANTS ++
var SCREEN_WIDTH        = 600;
var SCREEN_HEIGHT       = 450;

var STATE_PAUSE = 1;
var STATE_RUN   = 2;
var STATE_END   = 3;

var TYPE_ITEM_BUBBLE            = 1;
var TYPE_ITEM_ENEMY             = 2;
var TYPE_ITEM_POWERUP           = 3;

var TOTAL_NUMBER_BUBBLES        = 20;
var TOTAL_NUMBER_ENEMIES        = 20;
var TOTAL_NUMBER_POWERUPS       = 1;

// ++ MEMBERS ++
var m_canvas;                          // Document canvas
var m_context;                          // Document context

var m_state = STATE_PAUSE;       // Main game state
var m_iterator = 0;                     
var m_lastTime = 0;                     // The time of the last frame
var m_difTime = 0;                      // The time spent between two frames
var m_askToLose = false;
var m_score = 0;

var m_mouseDown;                        // Reports when the mouse is being pressed
var m_mousePositionX;           // X coordinate of the pressed mouse
var m_mousePositionY;           // Y coordinate of the pressed mouse

// BUTTONS
var m_playWidthMouse;
var m_playWidthKeyboard;
var m_pressHereToPlayAgain;

// IMAGES
var m_pinguImage;
var m_bubbleImage;
var m_virusImages = [];
var m_msdImages = [];


// ACTORS
var m_ball; 

var m_bubbles;                                                  // List of bubbles
var m_bubbleTimeGeneration=0;                   // Time acumulator
var m_bubbleTimeGeneration_Limit=2000;  // Time needed between two bubble generations
var m_bubbleTimeGeneration_Factor=1.1;  // Decrease factor in the generation bubble time

var m_enemies;
var m_enemyTimeGeneration=0;                    // Time acumulator
var m_enemyTimeGeneration_Limit=2000;   // Time needed between two enemies generations
var m_enemyTimeGeneration_Factor=0.8;   // Time decrease factor in the generation of an enemy 

var m_powerups;
var m_powerupTimeGeneration=0;                    // Time acumulator
var m_powerupTimeGeneration_Limit=8000;   // Time needed between two enemies generations
var m_powerupTimeGeneration_Factor=1.5;   // Time decrease factor in the generation of an enemy 

var m_level=1;                                                  // Game level
var m_timeLevelUp=0;                                    // Time acumulator
var m_timeLevelUp_Limit = 15000;                // Time to reach a level up
var m_timeLevelUp_Factor = 1.1;                 // Grow time factor for level up
var m_timeoutLevelUpTitle = 0;                  // Grow time factor for level up

var m_timeoutZap = 0;
var m_indexZap = 0;
var m_pulseZap = 1;

var m_timeoutInvulnerable = 0;
var m_indexInvulnerable = 0;
var m_pulseInvulnerable = 4;

var m_timeoutBulletTime = 0;
var m_bulletTimeMultiplier = 0.33;

var m_timeoutGravityWell = 0;
var m_gravityWellThreshold = 150;
var m_gravityWellMultiplier = 0.04;

// INPUT
var m_keyPressedLeft=false;
var m_keyPressedRight=false;
var m_keyPressedUp=false;
var m_keyPressedDown=false;
var friction=0.9;

// ------------------------------------
// OnKeyDown
document.onkeydown = function(event) 
{
     var key_pressed; 
     if(event == null)
         {
          key_pressed = window.event.keyCode; 
     }
     else 
         {
          key_pressed = event.keyCode; 
     }
     switch(key_pressed)
         {
          case 32:
               pause();
               break; 
          case 37:
               m_keyPressedLeft=true;
               break; 
          case 38:
               m_keyPressedUp=true;
               break; 
          case 39:
               m_keyPressedRight=true;
               break;
          case 40:
               m_keyPressedDown=true;
               break; 
     } 
}

// ------------------------------------
// OnKeyUp
document.onkeyup = function(event) 
{
     var key_pressed; 
     if(event == null)
         {
          key_pressed = window.event.keyCode; 
     }
     else 
         {
          key_pressed = event.keyCode; 
     }
     switch(key_pressed)
         {
          case 37:
               m_keyPressedLeft=false;
               break; 
          case 38:
               m_keyPressedUp=false;
               break; 
          case 39:
               m_keyPressedRight=false;
               break;
          case 40:
               m_keyPressedDown=false;
               break; 
     } 
}

// ------------------------------------
// Ask to create a new bubble
function AskNewBubble()
{
        var i=0;
        for (i=0;i<TOTAL_NUMBER_BUBBLES;i++)
        {
                var sBub = m_bubbles[i];
                if (!sBub.isAlive)
                {
                        sBub.Init(10, 20, 2, 10);
                        break;
                }
        }
}

// ------------------------------------
// Ask to create a new enemy
function AskNewEnemy()
{
        var i=0;
        for (i=0;i<TOTAL_NUMBER_ENEMIES;i++)
        {
                var sEnemy = m_enemies[i];
                if (!sEnemy.isAlive)
                {
                        sEnemy.Init(10, 20, 1, 5);
                        break;
                }
        }
}

// ------------------------------------
// Ask to create a new powerup
function AskNewPowerup()
{
        var i=0;
        for (i=0;i<TOTAL_NUMBER_POWERUPS;i++)
        {
                var sPow = m_powerups[i];
                if (!sPow.isAlive)
                {
                        sPow.Init(15, 15, 1, 1);
                        break;
                }
        }
}


// ------------------------------------
// Executes the logic of the bubbles increasing the level when a time limit is done
function UpdateItems()
{
        var i;
        
        // TIME GENERATOR BUBBLES
        m_bubbleTimeGeneration+=m_difTime;
        if (m_bubbleTimeGeneration>m_bubbleTimeGeneration_Limit)
        {
                m_bubbleTimeGeneration=0;
                if (m_timeoutLevelUpTitle<=0) AskNewBubble();
        }
        
        // UPDATE BUBBLE
        for (i=0;i<TOTAL_NUMBER_BUBBLES;i++)
        {
                m_bubbles[i].Update();
        }

        // TIME GENERATOR ENEMIES
        m_enemyTimeGeneration+=m_difTime;
        if (m_enemyTimeGeneration>m_enemyTimeGeneration_Limit)
        {
                m_enemyTimeGeneration=0;                
                if (m_timeoutLevelUpTitle<=0) AskNewEnemy();
        }
        
        // UPDATE ENEMIES
        for (i=0;i<TOTAL_NUMBER_ENEMIES;i++)
        {
                m_enemies[i].Update();
        }

        // TIME GENERATOR POWERUPS
        m_powerupTimeGeneration+=m_difTime;
        if (m_powerupTimeGeneration>m_powerupTimeGeneration_Limit)
        {
                m_powerupTimeGeneration=0;                
                if (m_timeoutLevelUpTitle<=0) AskNewPowerup();
        }

        // UPDATE POWERUPS
        for (i=0;i<TOTAL_NUMBER_POWERUPS;i++)
        {
                m_powerups[i].Update();
        }
}

// ------------------------------------
// Render the current alive bubbles
function RenderItems()
{
        var i;

        // UPDATE BUBBLE
        for (i=0;i<TOTAL_NUMBER_BUBBLES;i++)
        {
                var sBub = m_bubbles[i];
                if (sBub.isAlive)
                {
                        sBub.Render();
                }
        }
        
        // UPDATE ENEMIES
        for (i=0;i<TOTAL_NUMBER_ENEMIES;i++)
        {
                var sEne = m_enemies[i];
                if (sEne.isAlive)
                {
                        sEne.Render();
                }
        }

        // UPDATE POWERUPS
        for (i=0;i<TOTAL_NUMBER_POWERUPS;i++)
        {
                var sPow = m_powerups[i];
                if (sPow.isAlive)
                {
                        sPow.Render();
                }
        }
}


// ------------------------------------
// Executes the logic of the bubbles increasing the level when a time limit is done
function ResetGame()
{
        var i;

        m_lastTime = 0;
        m_difTime = 0;
        m_askToLose = false;
        m_score = 0;
        $('.score-text').text(m_score);

        m_bubbleTimeGeneration=0;
        m_bubbleTimeGeneration_Limit=2000;
        m_bubbleTimeGeneration_Factor=1.1;

        m_enemyTimeGeneration=0;
        m_enemyTimeGeneration_Limit=2000;
        m_enemyTimeGeneration_Factor=0.8;

        m_level=1;
        m_timeLevelUp=0;
        m_timeLevelUp_Limit = 15000;
        m_timeLevelUp_Factor = 1.1;
        m_timeoutLevelUpTitle = 0;
        m_timeoutZap = 0;
        m_timeoutInvulnerable = 0;
        m_timeoutBulletTime = 0;
        m_timeoutGravityWell = 0;
        $('.level-text').text(m_level);
        
        m_mousePositionX=0;
        m_mousePositionY=0;
        m_mouseDown = false; 
        
        m_ball.ResetBall();
        
        for (i=0;i<TOTAL_NUMBER_BUBBLES;i++)
        {
                m_bubbles[i].ResetItem();
        }
        
        
        for (i=0;i<TOTAL_NUMBER_ENEMIES;i++)
        {
                m_enemies[i].ResetItem();
        }

        for (i=0;i<TOTAL_NUMBER_POWERUPS;i++)
        {
                m_powerups[i].ResetItem();
        }

        m_ball.x = SCREEN_WIDTH/2;
        m_ball.y = SCREEN_HEIGHT/2;

        m_timeoutLevelUpTitle = 2000;
}

// ------------------------------------
// Change the main state of the game
function ChangeState(newState)
{
        m_state = newState;
        m_iterator = 0;
}

// ------------------------------------
// Main logic loop
function Update()
{
        if (m_iterator<100) m_iterator++;

        // CALCULATE TIME 
        var currentTime = new Date()
        if (m_lastTime == 0)
        {
                m_lastTime=currentTime.getTime();
                m_difTime=0;
        }
        else
        {
                m_difTime = currentTime.getTime() - m_lastTime;         
                m_lastTime = currentTime.getTime();
        }

        // LEVEL UP
        m_timeLevelUp+=m_difTime;
        if (m_timeLevelUp>m_timeLevelUp_Limit)
        {
                m_level++;
                m_timeoutLevelUpTitle = 2000;
                $('.level-text').text(m_level);
                m_timeLevelUp = 0;              
                m_timeLevelUp_Limit*=m_timeLevelUp_Factor;
                
                // CHANGE BUBBLE RATE
                m_bubbleTimeGeneration_Limit*=m_bubbleTimeGeneration_Factor;
        
                // CHANGE ENEMY RATE
                m_enemyTimeGeneration_Limit*=m_enemyTimeGeneration_Factor;
        }
        if (m_timeoutLevelUpTitle>0)
        {
                m_timeoutLevelUpTitle -= m_difTime;
        }

        // POWERUP COUNTERS
        if (m_timeoutZap>0) {
                m_timeoutZap -= m_difTime;
        }
        if (m_timeoutInvulnerable>0) {
                m_timeoutInvulnerable -= m_difTime;
        }
        if (m_timeoutBulletTime>0) {
                m_timeoutBulletTime -= m_difTime;
        }
        if (m_timeoutGravityWell>0) {
                m_timeoutGravityWell -= m_difTime;
        }

        // EVALUATE MAIN GAME STATE
        switch (m_state)
        {
                /////////////////////////
                case STATE_PAUSE:
                        break;
                
                /////////////////////////
                case STATE_RUN:
                        // ++ LOGIC ++
                        m_ball.Update();
                        UpdateItems();
                        
                        // ++ RENDER ++
                        m_context.clearRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

                        if( m_timeoutZap > 0 && m_indexZap++ > m_pulseZap) {
                          m_context.save();
                          m_context.fillStyle = '#cccccc';
                          m_context.fillRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
                          m_context.restore();
                        }
                        if(m_indexZap > 2 * m_pulseZap) m_indexZap = 0;

                        m_ball.Render();
                        RenderItems();

                        // Update background
                        if(++bgPos >= 500) bgPos = 0;
                        $('#gameCanvas').css('background-position', '0px ' + bgPos + 'px');

                        // RENDER SCORE
                        /*var titleScore = new MyText('SCORE: '+m_score,10,SCREEN_HEIGHT-30,24,'#ffffff', '20px sans-serif', 'left', 'top', false);
                        titleScore.Render();*/
                        
                        // LEVEL UP TITLE
                        /*if (m_timeoutLevelUpTitle>0)
                        {
                                var titleLevelUp = new MyText('LEVEL '+m_level, SCREEN_WIDTH/2, (SCREEN_HEIGHT/4), 24, '#ffffff', '50px sans-serif', 'center', 'top', false);
                                titleLevelUp.Render();
                        }*/
                        
                        // ++ END CONDITIONS ++
                        if (m_askToLose)
                        {
                                gameover();
                        }
                        break;
                        
                /////////////////////////
                case STATE_END:
                        break;
        }
}

function game_resize() {
    var _height,
        _width;
    if (typeof FlashCanvas != "undefined") {
      m_canvasOffsetTop  = $('.header').height() + 40;
      m_canvasOffsetLeft = $('#gameCanvas').offset().left;
      return;
    }

    _height = Math.floor(0.8 * window.innerHeight) - 100;
    _width = $('#gameCanvasWrap').width() - 50;

    SCREEN_HEIGHT = _height;
    SCREEN_WIDTH = _width;

    $('#gameCanvas').attr('height', _height);
    $('#gameCanvas').attr('width', _width);

    m_canvasOffsetTop = $('#gameCanvas').offset().top;
    m_canvasOffsetLeft = $('#gameCanvas').offset().left;
}
window.onresize = game_resize;

function gameover(){
  m_askToLose=true;
  $('.aviso').hide();
  stop();
  $('#pause-button .label').text('JUGAR!');
  $('#pause-button img').attr('src', 'img/pingu_go.png');
  $('#avisoGameOver').show();
  fill_twitter(m_level, m_score);
  if(!muted && typeof(FlashCanvas) == "undefined") $('audio')[0].pause();
  $('#gameOverModal').modal('show');
  //draw();
  interval = undefined;
  m_iterator = 0;
  m_state = STATE_END;
}

function pause(){
    $('.aviso').hide();
    if(m_state === STATE_END) {
        ResetGame();
        m_mouseDown=false;
        m_iterator = 0;
        start();
        $('#pause-button .label').text('Pausar');
        $('#pause-button img').attr('src', 'img/pingu_pause.png');
        m_state = STATE_RUN;
        if(!muted && typeof(FlashCanvas) == "undefined") $('audio')[0].play();
        $('.modal').modal('hide');
    } else if (interval){
        stop();
        $('#pause-button .label').text('JUGAR!');
        $('#pause-button img').attr('src', 'img/pingu_go.png');
        $('#avisoPausa').show();
        //draw();
        interval = undefined;
        if(!muted && typeof(FlashCanvas) == "undefined") jsfx_samples.pause.play();
        if(!muted && typeof(FlashCanvas) == "undefined") $('audio')[0].pause();
        m_state = STATE_PAUSE;
    } else {
        start();
        $('#pause-button .label').text('Pausar');
        $('#pause-button img').attr('src', 'img/pingu_pause.png');
        m_mouseDown=false;
        m_state = STATE_RUN;
        if(!muted && typeof(FlashCanvas) == "undefined") jsfx_samples.pause.play();
        if(!muted && typeof(FlashCanvas) == "undefined") $('audio')[0].play();
        $('.modal').modal('hide');
    }
}


function start() {
  interval = setInterval(Update, 1000/FPS);
}

function stop(){
  clearInterval(interval);
  interval = undefined;
}

function init() {
  // INIT GRAPHICS
  m_canvas = $('#gameCanvas').get(0);
  if (typeof FlashCanvas != "undefined") {
    FlashCanvas.initElement(m_canvas);
  }
  m_context=m_canvas.getContext('2d');

  // LOAD IMAGES
  m_pinguImage = new Image();
  m_pinguImage.src = 'img/pingu_bubble.png';

  m_bubbleImage = new Image();
  m_bubbleImage.src = 'img/bubble.png';
  
  m_virusImages[0] = new Image();
  m_virusImages[0].src = 'img/virus/green.png';
  m_virusImages[1] = new Image();
  m_virusImages[1].src = 'img/virus/orange.png';
  m_virusImages[2] = new Image();
  m_virusImages[2].src = 'img/virus/purple.png';
  m_virusImages[3] = new Image();
  m_virusImages[3].src = 'img/virus/yellow.png';

  m_msdImages[0] = new Image();
  m_msdImages[0].src = 'img/powerup/ray.png';
  m_msdImages[1] = new Image();
  m_msdImages[1].src = 'img/powerup/msdgreen.png';
  m_msdImages[2] = new Image();
  m_msdImages[2].src = 'img/powerup/magnet.png';
  m_msdImages[3] = new Image();
  m_msdImages[3].src = 'img/powerup/hourglass.png';
  
  // INIT GAME ELEMENTS
  m_ball = new Ball();
  
  // BUBBLES INIT
  m_bubbles=new Array(TOTAL_NUMBER_BUBBLES);
  var i;
  for (i=0;i<TOTAL_NUMBER_BUBBLES;i++) m_bubbles[i]=new Item(TYPE_ITEM_BUBBLE);
  
  // BUBBLES ENEMIES
  m_enemies=new Array(TOTAL_NUMBER_ENEMIES);
  for (i=0;i<TOTAL_NUMBER_ENEMIES;i++) m_enemies[i]=new Item(TYPE_ITEM_ENEMY);

  // BUBBLES POWERUPS
  m_powerups=new Array(TOTAL_NUMBER_POWERUPS);
  for (i=0;i<TOTAL_NUMBER_POWERUPS;i++) m_powerups[i]=new Item(TYPE_ITEM_POWERUP);
  
  // ENABLE MOUSE LISTENER
  m_mouseDown = false;

  function f_mouseDown(event) {
	  event.preventDefault();
          m_mouseDown = true; 
          m_mousePositionX=event.clientX - m_canvasOffsetLeft;
          m_mousePositionY=event.clientY - m_canvasOffsetTop;
  }
  function f_mouseDownTouch(event) {
	  event.preventDefault();
	  event.stopPropagation();
	  var p_event = event.originalEvent ? event.originalEvent : event;
	  p_event = p_event && p_event.targetTouches ? p_event.targetTouches[0] : event;
          m_mouseDown = true; 
          m_mousePositionX=p_event.pageX - m_canvasOffsetLeft;
          m_mousePositionY=p_event.pageY - m_canvasOffsetTop;
	  console.log('DOWN: Estoy en ' +  m_mousePositionX + ', ' +  m_mousePositionY );
/*
	  for(p in event) {
	    console.log('event[' + p + ']: ' + event[p]);
	  }
	  console.log('========================');
*/
  }
  function f_mouseUp(event) {
	  event.preventDefault();
    m_mouseDown = false; 
  }
  function f_mouseMove(event) {
    event.preventDefault();
    if (m_mouseDown) {
          m_mousePositionX=event.clientX - m_canvasOffsetLeft;
          m_mousePositionY=event.clientY - m_canvasOffsetTop;
     }
  }
  function f_mouseMoveTouch(event) {
    event.preventDefault();
    event.stopPropagation();
	  var p_event = event.originalEvent ? event.originalEvent : event;
	  p_event = p_event && p_event.targetTouches ? p_event.targetTouches[0] : event;
    if (m_mouseDown) {
          m_mousePositionX=p_event.pageX - m_canvasOffsetLeft;
          m_mousePositionY=p_event.pageY - m_canvasOffsetTop;
     }
  }

  // Mouse clicks and moves
  $('#gameCanvasWrap').on('mousedown', f_mouseDown);
  $('#gameCanvasWrap').on('mouseup', f_mouseUp);
  $('#gameCanvasWrap').on('mousemove', f_mouseMove);

  // Touch
  $('#gameCanvasWrap').on('touchstart', f_mouseDownTouch);
  $('#gameCanvasWrap').on('touchend', f_mouseUp);
  $('#gameCanvasWrap').on('touchcancel', f_mouseUp);
  $('#gameCanvasWrap').on('touchmove', f_mouseMoveTouch);

  game_resize();
  ResetGame();
  ChangeState(STATE_PAUSE);
}

init();

$('#pause-button').on('click', function() {
    pause();
    return false;
});

$('.modal').on('shown.bs.modal', function(){
  if (interval){
    pause();
  }
});

$(window).on('blur', function(){
  if (interval && typeof(FlashCanvas) == "undefined"){
    pause();
  }
});

$('.modal').on('hidden.bs.modal', function(){
  if (interval){
    //draw();
  } else {
    pause();
  }
});

$('#mute_button').on('click', function() {
  muted = !muted;
  $(this).find('span').toggle();
  if(muted && typeof(FlashCanvas) == "undefined") $('audio')[0].pause();
  else if(interval && typeof(FlashCanvas) == "undefined") $('audio')[0].play();
});

$('#helpModal .carousel').on('slide.bs.carousel', function (e) {
  for(i in e.relatedTarget.childNodes) {
    if(e.relatedTarget.childNodes[i]['className'] && e.relatedTarget.childNodes[i]['className'] == 'explicacion-lateral') {
      $('#helpModal .help-text').html(e.relatedTarget.childNodes[i].innerHTML);
    }
  }
})

}())

